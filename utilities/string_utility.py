class StringUtility:
    """Класс-утилита. Хранит функции для работы со строками"""

    @staticmethod
    def bold_text(text: str):
        """
        Возвращает текст полужирным

        :param text: текст
        :return: полужирный текст
        """
        return f"<b>{text}</b>"

    @staticmethod
    def code_text(text: str):
        """
        Возвращает текст для удобного копирования пользователем

        :param text: текст
        :return: копируемый текст
        """
        return f"<code>{text}</code>"
