from bot import bot
from data.config import Config


class CommonUtility:
    """Класс-утилита. Хранит общие функции"""

    @staticmethod
    async def inform_admins(message: str):
        """
        Информирует администраторов

        :param message: сообщение
        """
        for admin_id in Config.ADMINS:
            await bot.send_message(admin_id, message)
