import random
import string


class RandomUtility:
    """Класс-утилита. Хранит функции, связанные с рандомом"""

    @staticmethod
    def get_order_id(length=18) -> str:
        """
        Возвращает случайный уникальный ID заказа

        :param length: длина ID (по умолчанию=18)
        :return: ID ордера
        """
        return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(length))
