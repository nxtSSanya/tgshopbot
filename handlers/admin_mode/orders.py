from aiogram.types import Message

from bot import db_shop
from payments.yokassa_payment import YookassaPayment
from utilities.string_utility import StringUtility


async def get_all_orders(message: Message):
    """
    Функция-обработчик для кнопки "Заказы" из меню администратора. Выводит все сделанные заказы

    :param message: сообщение администратора
    """
    orders = db_shop.get_all_orders()

    for order in orders:
        text = f"{StringUtility.bold_text('Пользователь')}: {order.user_id}\n" \
               f"{StringUtility.bold_text('Номер заказа')}: {StringUtility.code_text(order.order_id)}\n" \
               f"{StringUtility.bold_text('Дата заказа')}: {order.date}\n" \
               f"{StringUtility.bold_text('Имя заказчика')}: {order.name}\n" \
               f"{StringUtility.bold_text('Телефон')}: {order.phone}\n" \
               f"{StringUtility.bold_text('Стоимость заказа')}: {order.payment_amount}\n" \
               f"{StringUtility.bold_text('Оплата')}: "

        if order.payment_id == '':
            text += 'наличными\n'
        else:
            text += 'онлайн\n' \
                    f"{StringUtility.bold_text('Статус оплаты')}: {await YookassaPayment.get_status_payment(order.payment_id)}\n"

        if order.address != '':
            text += f"{StringUtility.bold_text('Адрес')}: {order.address}"

        await message.answer(text, parse_mode='HTML')
