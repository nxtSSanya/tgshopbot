from aiogram.types import Message

from bot import db_shop


async def get_all_questions(message: Message):
    """
    Функция-обработчик кнопки "Вопросы" из меню администратора. Выводит все заданные пользователями вопросы

    :param message: сообщение администратора
    """
    questions = db_shop.get_all_questions()

    for question in questions:
        await message.answer(str(question), parse_mode='HTML')
