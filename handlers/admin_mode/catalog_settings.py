from aiogram import F
from aiogram.filters import Command
from aiogram.filters.callback_data import CallbackData
from aiogram.fsm.context import FSMContext
from aiogram.types import Message, ReplyKeyboardRemove, BufferedInputFile, CallbackQuery
from aiogram.utils.keyboard import InlineKeyboardBuilder

from bot import dp, db_shop
from databases.db_shop.tables.categories import Categories
from databases.db_shop.tables.products import Products
from helpers.markup import make_row_keyboard
from helpers.state_constants import select_type_setting, available_decline_states, add_product_state, all_states, \
    available_confirm_states, remove_product_state
from mode.admin import IsAdmin
from states.catalog_settings_state import CatalogSettingsState

product: Products
all_products = list[Products]
all_categories = list[Categories]


async def choose_type_setting(message: Message, state: FSMContext):
    """
    Функция-обработчик кнопки из меню администратора "Настройка каталога". Инициирует настройку каталога

    :param message: сообщение администратора
    :param state: переменная состояния для переключения состояния
    """
    await message.answer("Выберите действие, которое хотите сделать с каталогом",
                         reply_markup=make_row_keyboard(select_type_setting), parse_mode='HTML')
    await state.set_state(CatalogSettingsState.choice)


@dp.message(Command("cancel"))
@dp.message(CatalogSettingsState.choice, F.text.in_(available_decline_states))
async def cmd_cancel(message: Message, state: FSMContext):
    """
    Функция-обработчик команды /cancel и кнопок из 'available_decline_states'. Завершает работу с настройкой каталога

    :param message: сообщение администратора
    :param state: переменная состояния для переключения состояния
    """
    await message.answer(text="Действие отменено", reply_markup=ReplyKeyboardRemove())
    await state.clear()


@dp.message(CatalogSettingsState.choice, ~F.text.in_(select_type_setting))
async def cmd_error_input(message: Message):
    """
    Функция-обработчик, если пользователь выбрал кнопку не из списка 'select_type_setting'

    :param message: сообщение администратора
    """
    await message.answer(text="Таких вариантов нет. Выберите одно из предложенных")


# region Добавить

@dp.message(CatalogSettingsState.choice, F.text == add_product_state)
async def start_add_product(message: Message, state: FSMContext):
    """
    Функция-обработчик для начала добавления нового продукта. Просит ввести имя нового продукта

    :param message: сообщение администратора
    :param state: переменная состояния для переключения состояния
    """
    global all_products, all_categories, product
    product = Products()
    all_products = db_shop.get_all_products()
    all_categories = db_shop.get_all_categories()

    await message.answer("Отлично! Давайте начнём добавлять товар.\n\n"
                         "P.S. Если вы передумали, то можете на любом этапе написать команду '/cancel'",
                         reply_markup=ReplyKeyboardRemove())
    await message.answer("Введите имя товара")
    await state.set_state(CatalogSettingsState.name)


@dp.message(CatalogSettingsState.name)
async def set_name_product(message: Message, state: FSMContext):
    """
    Функция-обработчик для проверки введённого названия нового товара.
    В случае корректного имени просит ввести категорию товара

    :param message: сообщение администратора
    :param state: переменная состояния для переключения состояния
    """
    if message.text in map(lambda prod: prod.name, all_products):
        await message.answer("Товар с таким именем уже есть. Повторите ввод")
    else:
        product.name = message.text
        await state.set_state(CatalogSettingsState.category)
        await message.answer("Отлично! Введите категорию товара",
                             reply_markup=make_row_keyboard(list(map(lambda cat: cat.title, all_categories))),
                             parse_mode='HTML')


@dp.message(CatalogSettingsState.category)
async def set_category_product(message: Message, state: FSMContext):
    """
    Функция-обработчик для проверки введённой категории товара.
    В случае корректной категории просит ввести описание товара.

    :param message: сообщение администратора
    :param state: переменная состояния для переключения состояния
    """
    if message.text not in map(lambda cat: cat.title, all_categories):
        await message.answer("Такой категории нет. Повторите ввод")
    else:
        product.type = message.text
        await message.answer("Отлично! Введите описание товара")
        await state.set_state(CatalogSettingsState.body)


@dp.message(CatalogSettingsState.body)
async def set_body_product(message: Message, state: FSMContext):
    """
    Функция-обработчик для ввода количества товара

    :param message: сообщение администратора
    :param state: переменная состояния для переключения состояния
    """
    product.body = message.text

    await message.answer("Отлично! Введите количество товара")
    await state.set_state(CatalogSettingsState.quantity)


@dp.message(CatalogSettingsState.quantity)
async def set_quantity(message: Message, state: FSMContext):
    """
    Функция-обработчик для проверки введённого количества нового товара.
    В случае корректного количества просит загрузить фотографию

    :param message: сообщение администратора
    :param state: переменная состояния для переключения состояния
    """
    if message.text.isdigit():
        product.quantity = message.text
        await message.answer("Отлично! Загрузите фотографию")
        await state.set_state(CatalogSettingsState.photo)
    else:
        await message.answer("Некорректный ввод. Повторите")


@dp.message(CatalogSettingsState.photo, F.photo)
async def set_photo(message: Message, state: FSMContext):
    """
    Функция-обработчик для проверки загруженной фотографии нового товара.
    В случае корректной загрузки фотографии просит ввести цену товара

    :param message: сообщение администратора
    :param state: переменная состояния для переключения состояния
    """
    product.photo = (await message.bot.download(message.photo[-1])).read()
    await message.answer("Отлично! Введите цену товара")
    await state.set_state(CatalogSettingsState.price)


@dp.message(CatalogSettingsState.photo, ~F.photo)
async def error_send_photo(message: Message):
    """
    Функция-обработчик случая, когда пользователь не загрузил фотографию

    :param message: сообщение администратора
    """
    await message.answer("Вы не отправили фотографию!")


@dp.message(CatalogSettingsState.price)
async def set_price(message: Message, state: FSMContext):
    """
    Функция-обработчик для проверки введённой цены нового товара.
    В случае корректной цены выводит все введённые данные для проверки

    :param message: сообщение администратора
    :param state: переменная состояния для переключения состояния
    """
    if message.text.isdecimal():
        product.price = message.text
        await message.answer("Отлично! Проверьте введённые данные")
        await show_info_product(message, product, make_row_keyboard(all_states))
        await state.set_state(CatalogSettingsState.is_confirm)
    else:
        await message.answer("Некорректный ввод. Повторите")


@dp.message(CatalogSettingsState.is_confirm, F.text.in_(available_decline_states))
async def process_decline(message: Message, state: FSMContext):
    """
    Функция-обработчик для случая, когда администратор хочет начать заполнение информации заново

    :param message: сообщение администратора
    :param state: переменная состояния для переключения состояния
    """
    await message.answer("Что-то не так? Заполните поля заново")
    await message.answer("Введите название товара", reply_markup=ReplyKeyboardRemove())
    await state.set_state(CatalogSettingsState.name)


@dp.message(CatalogSettingsState.is_confirm, F.text.in_(available_confirm_states))
async def process_success(message: Message, state: FSMContext):
    """
    Функция-обработчик для успешного добавления товара

    :param message: сообщение администратора
    :param state: переменная состояния для переключения состояния
    """
    db_shop.add_new_product(product)
    await message.answer("Отлично! Товар добавлен", reply_markup=ReplyKeyboardRemove())
    await state.clear()


# endregion


# region Удалить

class DeleteCallback(CallbackData, prefix="cb"):
    """
    Класс для Callback для inline-кнопки при удалении продукта

    Атрибуты:
    ----------

    product_id (int): ID удаляемого продукта
    """
    product_id: int


@dp.message(CatalogSettingsState.choice, F.text == remove_product_state)
async def start_remove_product(message: Message, state: FSMContext):
    """
    Функция-обработчик для кнопки из меню администратора "Удалить". Выводит все товары с inline-кнопкой "Удалить"

    :param message: сообщение администратора
    :param state: переменная состояния для переключения состояния
    """
    products = db_shop.get_all_products()

    if len(products) == 0:
        await message.answer('Товаров нет', reply_markup=ReplyKeyboardRemove())
    else:
        await message.answer("Будем удалять товары", reply_markup=ReplyKeyboardRemove())

        for prod in products:
            builder = InlineKeyboardBuilder()
            builder.button(text=f'Удалить', callback_data=DeleteCallback(product_id=prod.product_id).pack())

            await show_info_product(message, prod, builder.as_markup())

    await state.clear()


@dp.callback_query(IsAdmin(), DeleteCallback.filter())
async def add_product_callback_handler(query: CallbackQuery, callback_data: DeleteCallback):
    """
    Функция обработки callback inline-кнопки "Удалить". Удаляет выбранный товар

    :param query: информация о нажавшем
    :param callback_data: данные о callback
    """
    db_shop.delete_product_by_id(callback_data.product_id)
    await query.answer("Продукт удалён!")
    await query.message.delete()


# endregion

async def show_info_product(message: Message, prod: Products, reply_markup):
    """
    Функция-утилита для вывода информации о продукте (с использованием __str__)

    :param message: сообщение администратора
    :param prod: продукт для вывода информации
    :param reply_markup: необходимый reply_markup
    """
    await message.answer_photo(photo=BufferedInputFile(prod.photo, "stub"),
                               caption=str(prod),
                               reply_markup=reply_markup, parse_mode="HTML")
