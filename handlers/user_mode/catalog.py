from typing import Sequence

from aiogram import F
from aiogram.filters.callback_data import CallbackData
from aiogram.types import Message, CallbackQuery, BufferedInputFile
from aiogram.utils.keyboard import InlineKeyboardBuilder

from bot import dp, db_shop
from data.config import Config
from databases.db_shop.tables.products import Products
from mode.user import IsUser
from utilities.string_utility import StringUtility


class CategoryCallback(CallbackData, prefix="cb"):
    """
    Класс для хранения информации о Callback при выводе каталога

    Атрибуты:
    ----------

    id (int): id категории товара
    action (str): действие с товаром
    """
    id: int
    action: str


async def process_catalog(message: Message):
    """
    Функция-обработчик для кнопки "Каталог". Просит выбрать категорию товаров

    :param message: сообщение пользователя
    """
    builder = InlineKeyboardBuilder()
    for category in db_shop.get_all_categories():
        builder.button(text=category.title,
                       callback_data=CategoryCallback(id=category.category_id, action="view").pack())
    await message.answer('Выберите раздел, чтобы вывести список товаров:', reply_markup=builder.as_markup())


@dp.callback_query(IsUser(), CategoryCallback.filter(F.action == "view"))
async def category_callback_handler(query: CallbackQuery, callback_data: CategoryCallback):
    """
    Функция-обработчик для вывода всех доступных товаров определённой категории

    :param query: информация о нажавшем
    :param callback_data: callback-информация
    """
    await query.answer('Все доступные товары.')
    await show_products(query.message, db_shop.get_products_from_not_cart(callback_data.id, query.message.chat.id))


@dp.callback_query(IsUser(), CategoryCallback.filter(F.action == "add"))
async def add_product_callback_handler(query: CallbackQuery, callback_data: CategoryCallback):
    """
    Функция-обработчик для случая, когда пользователь добавляет товар в корзину из каталога

    :param query: информация о нажавшем
    :param callback_data: callback-информация
    """
    item_price = db_shop.get_product_by_id(callback_data.id).price

    db_shop.add_cart_item(query.message.chat.id, callback_data.id, item_price)

    await query.answer('Товар добавлен в корзину!')
    await query.message.delete()


async def show_products(m, products: Sequence[Products]):
    """
    Функция-утилита для вывода товаров из каталога

    :param m: сообщение пользователя
    :param products: список продуктов для вывода
    """
    if len(products) == 0:
        await m.answer('Здесь ничего нет 😢')
    else:
        for product in products:
            builder = InlineKeyboardBuilder()
            builder.button(text=f'Добавить в корзину - {product.price} {Config.CURRENCY}',
                           callback_data=CategoryCallback(id=product.product_id, action="add").pack())
            text = f'{StringUtility.bold_text(product.name)}\n\n{product.body}\n\nКоличество товара на складе: {product.quantity}'

            await m.answer_photo(photo=BufferedInputFile(product.photo, "stub"),
                                 caption=text,
                                 reply_markup=builder.as_markup(), parse_mode="HTML")
