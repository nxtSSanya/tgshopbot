from aiogram import types
from aiogram.types import Message

# region user_mode

catalog = 'Каталог'
cart = 'Корзина'
make_offer = 'Оформить заказ'
users_orders = 'Заказы'

# endregion

# region admin_mode

catalog_settings = 'Настройка каталога'
orders = 'Заказы'
questions = 'Вопросы'

# endregion

order_info = 'Информация о заказе'


async def admin_menu(message: Message):
    """
    Функция обработчик команды '/menu'. Выводит админское меню

    :param message: сообщение администратора
    """
    kb = [
        [types.KeyboardButton(text=catalog_settings)],
        [types.KeyboardButton(text=questions)],
        [types.KeyboardButton(text=orders)],
        [types.KeyboardButton(text=order_info)]
    ]
    keyboard = types.ReplyKeyboardMarkup(keyboard=kb)

    await message.answer('Меню администратора', reply_markup=keyboard)


async def user_menu(message: Message):
    """
    Функция обработчик команды '/menu'. Выводит пользовательское меню

    :param message: сообщение пользователя
    """
    kb = [
        [types.KeyboardButton(text=catalog)],
        [types.KeyboardButton(text=make_offer)],
        [types.KeyboardButton(text=cart)],
        [types.KeyboardButton(text=users_orders)],
        [types.KeyboardButton(text=order_info)]
    ]
    keyboard = types.ReplyKeyboardMarkup(keyboard=kb)

    await message.answer('Пользовательское меню', reply_markup=keyboard)
