from contextlib import suppress

from aiogram import F
from aiogram.exceptions import TelegramBadRequest
from aiogram.filters import Command
from aiogram.filters.callback_data import CallbackData
from aiogram.types import Message, CallbackQuery, BufferedInputFile
from aiogram.utils.keyboard import InlineKeyboardBuilder

from bot import db_shop
from bot import dp
from data.config import Config
from mode.user import IsUser
from utilities.string_utility import StringUtility


class BuyCallback(CallbackData, prefix="cb"):
    """
    Класс для хранения информации о callback при действиях с корзиной

    Атрибуты:
    ----------

    product_id (int): ID продукта

    count (int): количество продуктов, которое нужно пользователю

    db_count (int): количество продуктов на складе

    action (str): действие, которое нужно осуществить с корзиной
    """
    product_id: int
    count: int
    db_count: int
    action: str


@dp.message(Command('cart'))
async def process_cart(message: Message):
    """
    Функция-обработчик команды /cart и кнопки "Корзина". Выводит всю корзину пользователя

    :param message: сообщение пользователя
    """
    items_cart = db_shop.get_not_ordered_items(message.chat.id)

    if len(items_cart) == 0:
        await message.answer('Ваша корзина пуста 😭')
    else:

        await message.answer(f'Стоимость корзины: {db_shop.get_cart_cost(message.chat.id)} {Config.CURRENCY}')

        for product_cart in items_cart:
            product = db_shop.get_product_by_id(product_cart.product_id)

            text = f'{StringUtility.bold_text(product.name)}\n\n{product.body}\n\nЦена: {product_cart.price} {Config.CURRENCY}.'

            builder = InlineKeyboardBuilder()
            builder.button(text='-', callback_data=BuyCallback(product_id=product_cart.product_id,
                                                               count=product_cart.quantity,
                                                               db_count=product.quantity,
                                                               action='decrease'))
            builder.button(text=str(product_cart.quantity),
                           callback_data=BuyCallback(product_id=product_cart.product_id, count=product_cart.quantity,
                                                     db_count=product.quantity, action='count'))
            builder.button(text='+',
                           callback_data=BuyCallback(product_id=product_cart.product_id, count=product_cart.quantity,
                                                     db_count=product.quantity, action='increase'))

            await message.answer_photo(photo=BufferedInputFile(product.photo, "stub"), caption=text,
                                       reply_markup=builder.as_markup(),
                                       parse_mode="HTML")


@dp.callback_query(IsUser(), BuyCallback.filter(F.action == "count"))
async def product_count_callback_handler(query: CallbackQuery, callback_data: BuyCallback):
    """
    Функция-обработчик inline-кнопки с количеством товара. Дублирует отдельным сообщением количество товара

    :param query: информация о нажавшем
    :param callback_data: callback-информация
    """
    await query.answer(f'Количество - {callback_data.count}')


@dp.callback_query(IsUser(), BuyCallback.filter(F.action == "increase"))
async def product_callback_handler(query: CallbackQuery, callback_data: BuyCallback):
    """
    Функция-обработчик inline-кнопки "+". Увеличивает количество товара в корзине

    :param query: информация о нажавшем
    :param callback_data: callback-информация
    :return:
    """
    product_id = callback_data.product_id
    cur_item_cart_count = callback_data.count
    db_item_cnt = callback_data.db_count

    if cur_item_cart_count >= db_item_cnt:
        await query.message.answer('Вы выбрали максимальное количество товара. У нас больше нет))')
    else:
        cur_item_cart_count += 1

    db_shop.update_cart(product_id, query.message.chat.id, cur_item_cart_count)
    builder = InlineKeyboardBuilder()
    builder.button(text='-',
                   callback_data=BuyCallback(product_id=product_id, count=cur_item_cart_count, db_count=db_item_cnt,
                                             action='decrease'))
    builder.button(text=str(cur_item_cart_count),
                   callback_data=BuyCallback(product_id=product_id, count=cur_item_cart_count, db_count=db_item_cnt,
                                             action='count'))
    builder.button(text='+',
                   callback_data=BuyCallback(product_id=product_id, count=cur_item_cart_count, db_count=db_item_cnt,
                                             action='increase'))
    with suppress(TelegramBadRequest):
        await query.message.edit_reply_markup(reply_markup=builder.as_markup())


@dp.callback_query(IsUser(), BuyCallback.filter(F.action == "decrease"))
async def product_callback_handler(query: CallbackQuery, callback_data: BuyCallback):
    """
    Функция-обработчик inline-кнопки "-". Уменьшает количество товара в корзине

    :param query: информация о нажавшем
    :param callback_data: callback-информация
    :return:
    """
    item_count_in_cart = callback_data.count
    item_count_in_db = callback_data.db_count
    product_id = callback_data.product_id

    if item_count_in_cart - 1 <= 0:
        db_shop.delete_from_cart_by_user_id(product_id, query.message.chat.id)
        await query.message.delete()
        return
    else:
        item_count_in_cart -= 1

    db_shop.update_cart(product_id, query.message.chat.id, item_count_in_cart)
    builder = InlineKeyboardBuilder()
    builder.button(text='-',
                   callback_data=BuyCallback(product_id=product_id, count=item_count_in_cart, db_count=item_count_in_db,
                                             action='decrease'))
    builder.button(text=str(item_count_in_cart),
                   callback_data=BuyCallback(product_id=product_id, count=item_count_in_cart, db_count=item_count_in_db,
                                             action='count'))
    builder.button(text='+',
                   callback_data=BuyCallback(product_id=product_id, count=item_count_in_cart, db_count=item_count_in_db,
                                             action='increase'))
    with suppress(TelegramBadRequest):
        await query.message.edit_reply_markup(reply_markup=builder.as_markup())
