from aiogram import F
from aiogram.filters import Command
from aiogram.filters.callback_data import CallbackData
from aiogram.fsm.context import FSMContext
from aiogram.types import Message, CallbackQuery
from aiogram.types import ReplyKeyboardRemove
from aiogram.utils.keyboard import InlineKeyboardBuilder

from bot import db_shop
from bot import dp
from data.config import Config
from handlers.user_mode.offer.offer_bl import offer_callback_pay, check_online_payment
from helpers.markup import make_row_keyboard
from helpers.state_constants import pay_decline_state, pay_accept_state, \
    pay_all_state, available_decline_states, \
    available_confirm_states, all_states
from mode.user import IsUser
from models.offer_model import OfferModel
from states import user_info_state
from utilities.string_utility import StringUtility


class OfferCallback(CallbackData, prefix="cb"):
    """
    Класс для хранения информации Callback о требовании доставки

    Атрибуты:
    ----------

    is_delivery (bool): нужна ли доставка
    """
    is_delivery: bool


offer_model = OfferModel()


async def make_delivery_handler(message: Message):
    """
    Функция-обработчик кнопки "Оформить заказ". Инициирует процесс оформления заказа

    :param message: сообщение пользователя
    """
    cart_cost = db_shop.get_cart_cost(message.chat.id)

    if cart_cost is not None:

        await message.answer(f'Стоимость заказа: {db_shop.get_cart_cost(message.chat.id)} {Config.CURRENCY}')

        builder = InlineKeyboardBuilder()
        builder.button(text='Заберу сам', callback_data=OfferCallback(is_delivery=False))
        builder.button(text='Не хочу идти, привезите', callback_data=OfferCallback(is_delivery=True))
        builder.adjust(1)
        await message.answer("Отлично! Самовывоз или оформим доставку?", reply_markup=builder.as_markup())
    else:
        await message.answer('Ваша корзина пуста 😭')


@dp.callback_query(IsUser(), OfferCallback.filter())
async def pickup_callback_handler(query: CallbackQuery, callback_data: OfferCallback, state: FSMContext):
    """
    Функция-обработчик для ввода имени пользователя

    :param query: информация о нажавшем по inline-кнопке
    :param callback_data: информация о callback
    :param state: переменная для переключения состояния
    """
    offer_model.is_delivery = callback_data.is_delivery

    await query.message.answer("Ок! Введите Ваше имя")
    await state.set_state(user_info_state.UserInfoState.name)
    await query.message.delete()


@dp.message(user_info_state.UserInfoState.name)
async def process_name(message: Message, state: FSMContext):
    """
    Функция-обработчик для ввода номера телефона

    :param message: сообщение пользователя
    :param state: переменная для переключения состояния
    """
    await message.answer("Ок! Введите Ваш номер телефона")

    offer_model.name = message.text

    await state.set_state(user_info_state.UserInfoState.phone)


@dp.message(user_info_state.UserInfoState.phone)
async def process_phone(message: Message, state: FSMContext):
    """
    Функция-обработчик для ввода адреса. Если без доставки, то выводит данные для проверки

    :param message: сообщение пользователя
    :param state: переменная для переключения состояния
    """
    available_ru_phone_sizes = [10, 11, 12]

    if len(message.text.lower()) in available_ru_phone_sizes:
        offer_model.phone = message.text
        if offer_model.is_delivery:
            await message.answer("Ок! Введите адрес доставки")
            await state.set_state(user_info_state.UserInfoState.address)
        else:
            await message.answer("Ок! Проверьте введенную информацию")

            await message.answer(f'{StringUtility.bold_text("Имя:")} {offer_model.name} \n\n'
                                 f'{StringUtility.bold_text("Номер телефона:")} {offer_model.phone}\n\n',
                                 reply_markup=make_row_keyboard(all_states), parse_mode='HTML')
            await state.set_state(user_info_state.UserInfoState.confirm)
    else:
        await message.answer("Вы ввели некорректный номер телефона, повторите ввод")
        await state.set_state(user_info_state.UserInfoState.phone)


@dp.message(user_info_state.UserInfoState.address)
async def process_address(message: Message, state: FSMContext):
    """
    Функция-обработчик введённого адреса. Выводит для проверки введённую информацию

    :param message: сообщение пользователя
    :param state: переменная для переключения состояния
    """
    await message.answer("Ок! Проверьте введенную информацию")

    offer_model.address = message.text

    await message.answer(f'{StringUtility.bold_text("Имя:")} {offer_model.name} \n\n'
                         f'{StringUtility.bold_text("Номер телефона:")} {offer_model.phone}\n\n'
                         f'{StringUtility.bold_text("Адрес:")} {offer_model.address}\n\n',
                         reply_markup=make_row_keyboard(all_states), parse_mode='HTML')
    await state.set_state(user_info_state.UserInfoState.confirm)


@dp.message(user_info_state.UserInfoState.confirm, F.text.in_(available_confirm_states))
async def process_confirmation(message: Message, state: FSMContext):
    """
    Функция-обработчик для информирования об успешном заполнении и вывода кнопок для выбора способа оплаты

    :param message: сообщение пользователя
    :param state: переменная для переключения состояния
    """
    await message.answer('Отлично!', reply_markup=ReplyKeyboardRemove())
    await message.answer('Перейдем к оплате', reply_markup=make_row_keyboard(pay_all_state))
    await state.set_state(user_info_state.UserInfoState.confirmed)


@dp.message(user_info_state.UserInfoState.confirm, F.text.in_(available_decline_states))
async def process_decline(message: Message, state: FSMContext):
    """
    Функция-обработчик для случая, когда пользователь хочет заново ввести информацию о себе

    :param message: сообщение пользователя
    :param state: переменная для переключения состояния
    """
    await message.answer("Что-то не так? Заполните поля заново")
    await message.answer("Введите Ваше имя", reply_markup=ReplyKeyboardRemove())
    await state.set_state(user_info_state.UserInfoState.name)


@dp.message(user_info_state.UserInfoState.confirm, lambda message: message.text not in all_states)
async def process_decline(message: Message):
    """
    Функция-обработчик для случая, когда пользователь вводит неожидаемое сообщение при подтверждении

    :param message: сообщение пользователя
    """
    await message.answer('Такого варианта не было.\n\n '
                         'Пожалуйста, выберите одно из названий из списка ниже:',
                         reply_markup=make_row_keyboard(all_states))


@dp.message(user_info_state.UserInfoState.confirmed, lambda message: message.text not in pay_all_state)
async def process_incorrect_confirmation(message: Message):
    """
    Функция-обработчик для случая, когда пользователь вводит неожидаемое сообщение при выборе оплаты

    :param message: сообщение пользователя
    """
    await message.answer('Такого варианта не было.\n\n '
                         'Пожалуйста, выберите одно из названий из списка ниже:',
                         reply_markup=make_row_keyboard(pay_all_state))


@dp.message(Command("cancel"))
@dp.message(F.text.lower() == "отмена")
@dp.message(user_info_state.UserInfoState.confirmed, F.text.in_(pay_decline_state))
async def cmd_cancel(message: Message, state: FSMContext):
    """
    Функция-обработчик команды /cancel, сообщения "отмена" и кнопок из 'pay_decline_state'. Отменяет оформление заказа

    :param message: сообщение пользователя
    :param state: переменная для переключения состояния
    """
    await state.clear()
    await message.answer(text="Действие отменено", reply_markup=ReplyKeyboardRemove())


@dp.message(user_info_state.UserInfoState.confirmed, F.text.in_(pay_accept_state))
async def pay_handler(message: Message, state: FSMContext):
    """
    Функция-обработчик при выборе способа оплаты

    :param message: сообщение пользователя
    :param state: переменная для переключения состояния
    """
    if message.text == "Оплатить онлайн":
        offer = await offer_callback_pay(message, offer_model, online_pay=True)
        await state.clear()
        await check_online_payment(message.chat.id, offer.payment_id, offer.order_id)
    elif message.text == "Оплатить наличными":
        await offer_callback_pay(message, offer_model, online_pay=False)
        await state.clear()
