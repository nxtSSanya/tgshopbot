import asyncio
from datetime import datetime, timedelta

from aiogram.types import Message, ReplyKeyboardRemove
from aiogram.utils.keyboard import InlineKeyboardBuilder

from bot import db_shop, bot
from data.config import Config
from enums.inform_type import InformType
from enums.payment_statuses import PaymentStatuses
from models.offer_model import OfferModel
from payments.yokassa_payment import YookassaPayment
from utilities.common_utility import CommonUtility
from utilities.string_utility import StringUtility


async def offer_callback_pay(message: Message, offer: OfferModel, online_pay: bool):
    """
    Функция-утилита, которая осуществляет окончательное оформление заказа и заносит его в базу данных

    :param message: сообщение пользователя
    :param offer: информация о заказе
    :param online_pay: оплачивается ли заказ онлайн
    """
    builder = InlineKeyboardBuilder()
    cart_cost = db_shop.get_cart_cost(message.chat.id)

    if online_pay:
        payment_response = await YookassaPayment.create_payment(cart_cost, Config.CURRENCY, Config.PAYMENT_TYPE,
                                                                Config.CONFIRMATION_TYPE, Config.BOT_URL,
                                                                Config.IS_CAPTURE, "f")  # TODO description
        order_id = db_shop.add_new_order(name=offer.name,
                                         phone=offer.phone,
                                         address=offer.address if offer.is_delivery else '',
                                         user_id=message.chat.id,
                                         payment_id=payment_response.id,
                                         payment_amount=cart_cost,
                                         date=datetime.now())
        builder.button(text="Ссылка для оплаты", url=payment_response.confirmation.confirmation_url)
        offer.payment_id = payment_response.id
        offer.order_id = order_id
    else:
        order_id = db_shop.add_new_order(name=offer.name,
                                         phone=offer.phone,
                                         address=offer.address if offer.is_delivery else '',
                                         user_id=message.chat.id,
                                         payment_id='',
                                         payment_amount=cart_cost,
                                         date=datetime.now())

    db_shop.update_quantity_after_offer(message.chat.id)
    db_shop.make_cart_ordered(message.chat.id, order_id)

    await CommonUtility.inform_admins(str(InformType.ORDER.value))
    await message.answer("Отлично!", reply_markup=ReplyKeyboardRemove())
    await message.answer(text=f"Номер вашего заказа: {StringUtility.code_text(order_id)}",
                         reply_markup=builder.as_markup(), parse_mode='HTML')

    return offer


async def check_online_payment(user_id: int, payment_id: str, order_id: str):
    """
    Функция-утилита, которая осуществляет мониторинг онлайн-оплаты пользователя.
    В любом случае информирует пользователя и администраторов

    :param user_id: ID пользователя
    :param payment_id: ID онлайн платежа
    :param order_id: ID заказа
    """
    end_date = datetime.now() + timedelta(minutes=Config.PAYMENT_TIMEOUT_MINUTE)

    while datetime.now() <= end_date:
        payment_status = await YookassaPayment.get_status_payment(payment_id)

        if payment_status == PaymentStatuses.SUCCEEDED.value:
            await bot.send_message(user_id, str(InformType.ORDER_SUCCEEDED.value).format(order_id))
            await CommonUtility.inform_admins(str(InformType.ORDER_SUCCEEDED.value).format(order_id))
            return

        if payment_status == PaymentStatuses.CANCELED.value:
            await bot.send_message(user_id, str(InformType.ORDER_CANCELED.value).format(order_id))
            await CommonUtility.inform_admins(str(InformType.ORDER_CANCELED.value).format(order_id))
            return

        await asyncio.sleep(Config.CHECK_PAYMENT_INTERVAL_SECONDS)

    await bot.send_message(user_id, str(InformType.ORDER_PAY_ERROR.value).format(order_id))
    await CommonUtility.inform_admins(str(InformType.ORDER_PAY_ERROR.value).format(order_id))
