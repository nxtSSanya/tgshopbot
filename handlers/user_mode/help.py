from datetime import datetime

from aiogram import F
from aiogram.filters import Command
from aiogram.fsm.context import FSMContext
from aiogram.types import Message
from aiogram.types import ReplyKeyboardRemove

from bot import db_shop
from bot import dp
from enums.inform_type import InformType
from helpers.markup import make_row_keyboard
from helpers.state_constants import all_states, available_confirm_states, available_decline_states
from mode.user import IsUser
from models.question_model import QuestionModel
from states.help_state import HelpState
from utilities.common_utility import CommonUtility

questionModel = QuestionModel()


@dp.message(Command('help'), IsUser())
async def cmd_help(message: Message, state: FSMContext):
    """
    Функция-обработчик команды '/help'. Инициирует процесс ввода вопроса. Просит ввести имя пользователя

    :param message: сообщение пользователя
    :param state: переменная для переключения состояния
    """
    await message.answer('Столкнулись с проблемой?\n'
                         'Представьтесь'
                         '\n\nP.S. Если передумали задавать вопрос - напишите "/cancel"')
    await state.set_state(HelpState.name)


@dp.message(HelpState.name)
async def process_name(message: Message, state: FSMContext):
    """
    Функция-обработчик для сохранения имени. Просит ввести номер телефона

    :param message: сообщение пользователя
    :param state: переменная для переключения состояния
    """
    questionModel.name = message.text

    await message.answer("Ок! Введите Ваш номер телефона")
    await state.set_state(HelpState.phone)


@dp.message(HelpState.phone)
async def process_phone(message: Message, state: FSMContext):
    """
    Функция-обработчик для сохранения номера телефона. Просит описать проблему

    :param message: сообщение пользователя
    :param state: переменная для переключения состояния
    """
    available_ru_phone_sizes = [10, 11, 12]

    if len(message.text.lower()) in available_ru_phone_sizes:
        questionModel.phone = message.text
        await message.answer("Ок! Опишите подробно свою проблему")
        await state.set_state(HelpState.question)
    else:
        await message.answer("Вы ввели некорректный номер телефона, повторите ввод")


@dp.message(HelpState.question)
async def process_question(message: Message, state: FSMContext):
    """
    Функция-обработчик для сохранения вопроса. Выводит введённую информацию для проверки

    :param message: сообщение пользователя
    :param state: переменная для переключения состояния
    """
    questionModel.question = message.text

    await message.answer("Ок! Проверьте введенную информацию")

    await message.answer(f'Убедитесь, что все верно.\n\n'
                         f'{str(questionModel)}',
                         reply_markup=make_row_keyboard(all_states), parse_mode='HTML')
    await state.set_state(HelpState.submit)


@dp.message(HelpState.submit, lambda message: message.text not in all_states)
async def process_incorrect_confirmation(message: Message):
    """
    Функция-обработчик сообщения, которого нет в 'all_states'

    :param message: сообщение пользователя
    """
    await message.answer('Такого варианта не было.\n\n '
                         'Пожалуйста, выберите одно из названий из списка ниже:',
                         reply_markup=make_row_keyboard(all_states))


@dp.message(Command("cancel"))
@dp.message(F.text.lower() == "отмена")
async def cmd_cancel(message: Message, state: FSMContext):
    """
    Функция-обработчик команды '/cancel' и сообщения 'отмена'

    :param message: сообщение пользователя
    :param state: переменная для переключения состояния
    """
    await message.answer(text="Действие отменено", reply_markup=ReplyKeyboardRemove())
    await state.clear()


@dp.message(HelpState.submit, F.text.in_(available_decline_states))
async def process_decline(message: Message, state: FSMContext):
    """
    Функция-обработчик для кнопок из 'available_decline_states'. Запускает процесс ввода информации заново

    :param message: сообщение пользователя
    :param state: переменная для переключения состояния
    """
    await message.answer('Ок, отменяем. Представьтесь', reply_markup=ReplyKeyboardRemove())
    await state.set_state(HelpState.name)


@dp.message(HelpState.submit, F.text.in_(available_confirm_states))
async def process_submit(message: Message, state: FSMContext):
    """
    Функция-обработчик успешного ввода. Добавляет вопрос в базу данных

    :param message: сообщение пользователя
    :param state: переменная для переключения состояния
    """
    db_shop.add_new_message(message.chat.id, questionModel.name, questionModel.phone, questionModel.question,
                            datetime.now())
    await CommonUtility.inform_admins(str(InformType.HELP.value))
    await message.answer('Ваш вопрос отправлен администратору!', reply_markup=ReplyKeyboardRemove())
    await state.clear()
