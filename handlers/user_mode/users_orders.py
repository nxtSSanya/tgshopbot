from aiogram.types import Message

from bot import db_shop
from enums.payment_statuses import PaymentStatuses
from payments.yokassa_payment import YookassaPayment
from utilities.string_utility import StringUtility


async def process_order_status(message: Message):
    """
    Функция-обработчик кнопки пользовательского меню "Заказы". Выводит все заказы пользователя

    :param message: сообщение пользователя
    """
    all_orders = db_shop.get_all_orders()

    if len(all_orders) == 0:
        await message.answer("У вас нет заказов")
        return

    await message.answer("Все ваши заказы:")

    for order in all_orders:

        if order.user_id != message.chat.id:
            continue

        text = f'Заказ: {StringUtility.code_text(order.order_id)}. '

        status = ''

        if order.payment_id != '':
            status = await YookassaPayment.get_status_payment(order.payment_id)
            text += f'Статус оплаты: {status}. '
        else:
            text += f'Ожидается оплата наличными. '

        if status == PaymentStatuses.CANCELED.value:
            text += f'Оплата не поступила. Заказ отменён'
        else:
            if order.address != '':
                text += f'Доставим по адресу: {order.address}'
            else:
                text += f'Самовывоз. Для получения заказа назовите последние 4 цифры заказа и код из СМС'

        await message.answer(text, parse_mode='HTML')
