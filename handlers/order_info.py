from aiogram import F
from aiogram.fsm.context import FSMContext
from aiogram.types import Message, ReplyKeyboardRemove, BufferedInputFile

from bot import dp, db_shop
from data.config import Config
from helpers.markup import make_row_keyboard
from helpers.state_constants import available_decline_states
from states.order_info_state import OrderInfoState


async def get_order_info(message: Message, state: FSMContext):
    """
    Функция-обработчик для кнопки "Информация о заказе" (и для администраторов, и для пользователей)

    :param message: сообщение пользователя
    :param state: переменная для переключения состояния
    """
    await message.answer("Введите номер заказа", reply_markup=make_row_keyboard(available_decline_states),
                         parse_mode='HTML')
    await state.set_state(OrderInfoState.order_id)


@dp.message(OrderInfoState.order_id, F.text.in_(available_decline_states))
async def cmd_cancel(message: Message, state: FSMContext):
    """
    Функция-обработчик для кнопок из 'available_decline_states'. Отменяет процесс получения информации о заказе

    :param message: сообщение пользователя
    :param state: переменная для переключения состояния
    """
    await state.clear()
    await message.answer(text="Действие отменено", reply_markup=ReplyKeyboardRemove())


@dp.message(OrderInfoState.order_id, ~F.text.regexp(r"\w{18}"))
async def wrong_order_id(message: Message):
    """
    Функция-обработчик для случая, когда введён некорректный номер заказ

    :param message: сообщение
    """
    await message.answer("Некорректный номер заказа. Повторите ввод")


@dp.message(OrderInfoState.order_id, F.text.regexp(r"\w{18}"))
async def check_order_id(message: Message, state: FSMContext):
    """
    Функция-обработчик ввода корректного номера заказа. Выводит информацию о заказе

    :param message: сообщение
    :param state: переменная для переключения состояния
    """

    items = db_shop.get_cart_items_by_order_id(message.text)

    if message.chat.id not in Config.ADMINS:
        items = list(filter(lambda item: item.user_id == message.chat.id, items))

    if len(items) != 0:

        for item in items:
            product = db_shop.get_product_by_id(item.product_id)
            text = f'Название товара: {product.name}\n' \
                   f'Цена за единицу: {product.price}\n' \
                   f'Количество в заказе: {item.quantity}\n' \
                   f'Описание: {product.body}'
            await message.answer_photo(photo=BufferedInputFile(product.photo, "stub"), caption=text)

        await message.answer(f'Общая стоимость заказа: {db_shop.get_cart_cost(message.chat.id, message.text)}')
        await state.clear()
    else:
        await message.answer("Несуществующий номер заказа. Повторите ввод",
                             reply_markup=make_row_keyboard(available_decline_states), parse_mode='HTML')
