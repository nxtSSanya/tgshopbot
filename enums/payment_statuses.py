from enum import Enum


class PaymentStatuses(Enum):
    """
    Enum для хранения статусов онлайн-заказа

    Атрибуты:
    ----------

    PENDING: статус после создания платежа, ожидает действий

    WAITING_FOR_CAPTURE: статус ожидания подтверждения (если в конфиге IS_CAPTURE=True)

    SUCCEEDED: статус после успешной оплаты

    CANCELED: статус отмены платежа
    """

    PENDING = "pending"
    WAITING_FOR_CAPTURE = "waiting_for_capture"
    SUCCEEDED = "succeeded"
    CANCELED = "canceled"
