from yookassa import Configuration, Payment
from yookassa.domain.response import PaymentResponse

from data.config import Config


class YookassaPayment:
    """
    Класс для работы с ЮKassa
    """

    Configuration.account_id = Config.ACCOUNT_ID
    Configuration.secret_key = Config.SECRET_KEY

    @staticmethod
    async def create_payment(amount_value: int, amount_currency: str, payment_method_data_type: str,
                             confirmation_type: str, confirmation_return_url: str, capture: bool,
                             description: str) -> PaymentResponse:
        """
        Создаёт онлайн-платёж

        :param amount_value: сумма заказа
        :param amount_currency: валюта заказа
        :param payment_method_data_type: способ оплаты
        :param confirmation_type: способ подтверждения
        :param confirmation_return_url: url для редиректа после оплаты
        :param capture: нужно ли подтверждение оплаты
        :param description: описание платежаы
        """
        return Payment.create({
            "amount": {
                "value": amount_value,
                "currency": amount_currency
            },
            "payment_method_data": {
                "type": payment_method_data_type
            },
            "confirmation": {
                "type": confirmation_type,
                "return_url": confirmation_return_url
            },
            "capture": capture,
            "description": description
        })

    @staticmethod
    async def get_status_payment(payment_id) -> str:
        """
        Возвращает статус платежа

        :param payment_id: ID платежа
        :return: статус платежа
        """
        return Payment.find_one(payment_id).status
