import asyncio
import logging

from aiogram import F
from aiogram import types
from aiogram.filters import Command
from aiogram.types import ReplyKeyboardRemove

from bot import dp, bot
from data.config import Config
from handlers.admin_mode.catalog_settings import choose_type_setting
from handlers.admin_mode.orders import get_all_orders
from handlers.admin_mode.questions import get_all_questions
from handlers.order_info import get_order_info
from handlers.user_mode.cart import process_cart
from handlers.user_mode.catalog import process_catalog
from handlers.user_mode.help import cmd_help
from handlers.user_mode.mode_menu import admin_menu, user_menu
from handlers.user_mode.mode_menu import catalog, cart, users_orders, make_offer, order_info, catalog_settings, orders, \
    questions
from handlers.user_mode.offer.offer import make_delivery_handler
from handlers.user_mode.users_orders import process_order_status
from mode.admin import IsAdmin
from mode.user import IsUser

user_message = 'Пользователь'
admin_message = 'Админ'
test_admin_mode = 'MakeMeAdmin'
test_user_mode = 'MakeMeUser'

dp.message.register(user_menu, Command('menu'), IsUser())
dp.message.register(cmd_help, Command('help'), IsUser())
dp.message.register(process_catalog, F.text == catalog, IsUser())
dp.message.register(process_cart, F.text == cart, IsUser())
dp.message.register(process_order_status, F.text == users_orders, IsUser())
dp.message.register(make_delivery_handler, F.text == make_offer, IsUser())

dp.message.register(admin_menu, Command('menu'), IsAdmin())
dp.message.register(choose_type_setting, F.text == catalog_settings, IsAdmin())
dp.message.register(get_all_questions, F.text == questions, IsAdmin())
dp.message.register(get_all_orders, F.text == orders, IsAdmin())

dp.message.register(get_order_info, F.text == order_info)


@dp.message(Command("start"))
async def cmd_start(message: types.Message):
    """
    Функция-обработчик для команды '/start'

    :param message: сообщение пользователя
    """
    await message.answer('''Привет!

Я ♂бот♂ онлайн магазина по продаже звукоизоляционный панелей.

Чтобы перейти в каталог и выбрать приглянувшиеся товары возпользуйтесь командой /menu.

Оплатить можно либо наличными, либо онлайн через ЮKassa
 
У нас существует доставка, либо вы можете сами забрать товар из пункта выдачи по адресу: улица Пушкина, дом 110
                                \nВремя работы:
                                \n<b>Пн-Пт: 10:00 - 18:00</b> 
                                \n<b>Сб: 10:00 - 14:00</b>
                                \n<b>Вс: выходной</b>

Возникли вопросы? Не проблема! Команда /help поможет связаться с админами, которые постараются как можно быстрее откликнуться.
    ''', parse_mode='HTML')


@dp.message(Command(test_user_mode))
async def user_mode(message: types.Message):
    """
    Функция-обработчик команды '/MakeMeUser'

    :param message: сообщение
    """
    cid = message.chat.id
    if cid in Config.ADMINS:
        Config.ADMINS.remove(cid)

    await message.answer('Включен тестовый пользовательский режим.', reply_markup=ReplyKeyboardRemove())


@dp.message(Command(test_admin_mode))
async def admin_mode(message: types.Message):
    """
    Функция-обработчик команды '/MakeMeAdmin'

    :param message: сообщение
    """
    cid = message.chat.id
    if cid not in Config.ADMINS:
        Config.ADMINS.append(cid)
        await message.answer('Включен тестовый админский режим.', reply_markup=ReplyKeyboardRemove())


@dp.message(Command(user_message))
async def user_mode(message: types.Message):
    cid = message.chat.id
    if cid not in Config.ADMINS:
        await message.answer('Включен пользовательский режим.', reply_markup=ReplyKeyboardRemove())
    else:
        await message.answer('Вы являетесь администратором.', reply_markup=ReplyKeyboardRemove())


@dp.message(Command(admin_message))
async def admin_mode(message: types.Message):
    if message.chat.id not in Config.ADMINS:
        await message.answer('Вы не являетесь администратором магазина.', reply_markup=ReplyKeyboardRemove())
    else:
        await message.answer('Включен админский режим.', reply_markup=ReplyKeyboardRemove())


@dp.message()
async def echo_message(message: types.Message):
    """
    Функция-обработчик любого неожидаемого сообщения

    :param message: сообщение пользователяы
    """
    if message.chat.id in Config.ADMINS:
        await message.answer('Список команд:\n\n /menu')
    else:
        await message.answer('Список команд:\n\n /menu \n\n /help')


async def main():
    """Функция для старта бота"""
    await dp.start_polling(bot)


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    asyncio.run(main())
