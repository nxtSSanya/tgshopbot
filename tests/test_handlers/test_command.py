import datetime
from typing import Any
from unittest.mock import AsyncMock

import pytest
from aiogram.handlers import MessageHandler
from aiogram.types import Chat, Message, User
from aiogram.types import ReplyKeyboardRemove

import data.config as cnf
import main


class MyHandler(MessageHandler):
    async def handle(self) -> Any:
        return self.event.text


@pytest.mark.asyncio
async def test_start_command():
    message = AsyncMock()
    await main.cmd_start(message)

    message.answer.assert_called_with('''Привет!

Я ♂бот♂ онлайн магазина по продаже звукоизоляционный панелей.

Чтобы перейти в каталог и выбрать приглянувшиеся товары возпользуйтесь командой /menu.

Оплатить можно либо наличными, либо онлайн через ЮKassa
 
У нас существует доставка, либо вы можете сами забрать товар из пункта выдачи по адресу: улица Пушкина, дом 110
                                \nВремя работы:
                                \n<b>Пн-Пт: 10:00 - 18:00</b> 
                                \n<b>Сб: 10:00 - 14:00</b>
                                \n<b>Вс: выходной</b>

Возникли вопросы? Не проблема! Команда /help поможет связаться с админами, которые постараются как можно быстрее откликнуться.
    ''', parse_mode='HTML')


@pytest.mark.asyncio
async def test_base_message():
    cid_mock = 1234567890
    event = Message(
        message_id=42,
        date=datetime.datetime.now(),
        text="test",
        chat=Chat(id=cid_mock, type="private"),
        from_user=User(id=cid_mock, is_bot=False, first_name="Test"),
    )
    handler = MyHandler(event=event)

    assert handler.from_user == event.from_user
    assert handler.chat == event.chat


@pytest.mark.asyncio
async def test_is_not_admin():
    cid_mock = 1234567890
    message_mock = AsyncMock(chat=Chat(id=cid_mock, type="private"))

    await main.admin_mode(message=message_mock)
    assert cid_mock not in cnf.Config.ADMINS
    message_mock.answer.assert_called_with('Вы не являетесь администратором магазина.', reply_markup=ReplyKeyboardRemove())


@pytest.mark.asyncio
async def test_is_admin():
    cid_mock = 854673388
    message_mock = AsyncMock(chat=Chat(id=cid_mock, type="private"))

    await main.admin_mode(message=message_mock)
    assert cid_mock in cnf.Config.ADMINS
    message_mock.answer.assert_called_with('Включен админский режим.', reply_markup=ReplyKeyboardRemove())


@pytest.mark.asyncio
async def test_echo_handler():
    text_mock = "test123"
    message_mock = AsyncMock(text=text_mock)
    await main.echo_message(message=message_mock)
    message_mock.answer.assert_called_with('Список команд:\n\n /menu \n\n /help')