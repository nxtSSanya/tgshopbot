from unittest.mock import AsyncMock

import pytest
from aiogram import types
from aiogram.types import Chat

import data.config as cnf
import main

# region test

catalog = 'Каталог'
cart = 'Корзина'
make_offer = 'Оформить заказ'
users_orders = 'Заказы'

catalog_settings = 'Настройка каталога'
orders = 'Заказы'
questions = 'Вопросы'

order_info = 'Информация о заказе'

# endregion

@pytest.mark.asyncio
async def test_is_admin_menu():
    cid_mock = 854673388
    message_mock = AsyncMock(chat=Chat(id=cid_mock, type="private"))
    kb = [
        [types.KeyboardButton(text=catalog_settings)],
        [types.KeyboardButton(text=questions)],
        [types.KeyboardButton(text=orders)],
        [types.KeyboardButton(text=order_info)]
    ]
    keyboard = types.ReplyKeyboardMarkup(keyboard=kb)

    await main.admin_menu(message=message_mock)
    assert cid_mock in cnf.Config.ADMINS
    message_mock.answer.assert_called_with('Меню администратора', reply_markup=keyboard)


@pytest.mark.asyncio
async def test_is_user_menu():
    cid_mock = 1234567890
    message_mock = AsyncMock(chat=Chat(id=cid_mock, type="private"))
    kb = [
        [types.KeyboardButton(text=catalog)],
        [types.KeyboardButton(text=make_offer)],
        [types.KeyboardButton(text=cart)],
        [types.KeyboardButton(text=users_orders)],
        [types.KeyboardButton(text=order_info)]
    ]
    keyboard = types.ReplyKeyboardMarkup(keyboard=kb)

    await main.user_menu(message=message_mock)
    assert cid_mock not in cnf.Config.ADMINS
    message_mock.answer.assert_called_with('Пользовательское меню', reply_markup=keyboard)