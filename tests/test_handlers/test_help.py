from unittest.mock import AsyncMock

import pytest
from aiogram.fsm.context import FSMContext
from aiogram.fsm.storage.base import StorageKey
from aiogram.types import Chat
from aiogram.types import ReplyKeyboardRemove

from handlers.user_mode.help import cmd_help, process_name, process_phone, process_question, \
    cmd_cancel, process_decline
from states import help_state
from tests.utils import TEST_USER_CHAT, TEST_USER


@pytest.mark.asyncio
async def test_help_state_name(storage, bot_):
    message = AsyncMock()
    state = FSMContext(
        storage=storage,
        key=StorageKey(bot_id=bot_.id, user_id=TEST_USER.id, chat_id=TEST_USER_CHAT.id)
    )

    await cmd_help(message, state=state)
    assert await state.get_data() == {}
    assert await state.get_state() == help_state.HelpState.name.state


@pytest.mark.asyncio
async def test_help_state_phone(storage, bot_):
    message = AsyncMock()
    state = FSMContext(
        storage=storage,
        key=StorageKey(bot_id=bot_.id, user_id=TEST_USER.id, chat_id=TEST_USER_CHAT.id)
    )

    await process_name(message, state=state)
    assert await state.get_data() == {}
    assert await state.get_state() == help_state.HelpState.phone.state


@pytest.mark.asyncio
async def test_help_state_phone_positive(storage, bot_):
    message = AsyncMock(text="1234567890", chat=Chat(id=TEST_USER_CHAT.id, type="private"))

    state = FSMContext(
        storage=storage,
        key=StorageKey(bot_id=bot_.id, user_id=TEST_USER.id, chat_id=TEST_USER_CHAT.id)
    )

    await process_phone(message, state=state)
    message.answer.assert_called_with("Ок! Опишите подробно свою проблему")
    assert await state.get_data() == {}
    assert await state.get_state() == help_state.HelpState.question.state


@pytest.mark.asyncio
async def test_help_state_phone_negative(storage, bot_):
    message = AsyncMock(text="12345", chat=Chat(id=TEST_USER_CHAT.id, type="private"))

    state = FSMContext(
        storage=storage,
        key=StorageKey(bot_id=bot_.id, user_id=TEST_USER.id, chat_id=TEST_USER_CHAT.id)
    )

    await process_phone(message, state=state)
    message.answer.assert_called_with("Вы ввели некорректный номер телефона, повторите ввод")
    assert await state.get_data() == {}
    assert await state.get_state() is None


@pytest.mark.asyncio
async def test_help_state_question(storage, bot_):
    message = AsyncMock()

    state = FSMContext(
        storage=storage,
        key=StorageKey(bot_id=bot_.id, user_id=TEST_USER.id, chat_id=TEST_USER_CHAT.id)
    )

    await process_question(message, state=state)

    assert await state.get_data() == {}
    assert await state.get_state() == help_state.HelpState.submit.state


@pytest.mark.asyncio
async def test_help_state_cancel(storage, bot_):
    message = AsyncMock()

    state = FSMContext(
        storage=storage,
        key=StorageKey(bot_id=bot_.id, user_id=TEST_USER.id, chat_id=TEST_USER_CHAT.id)
    )

    await cmd_cancel(message, state=state)

    message.answer.assert_called_with(text="Действие отменено", reply_markup=ReplyKeyboardRemove())

    assert await state.get_data() == {}
    assert await state.get_state() is None


@pytest.mark.asyncio
async def test_help_state_decline(storage, bot_):
    message = AsyncMock()

    state = FSMContext(
        storage=storage,
        key=StorageKey(bot_id=bot_.id, user_id=TEST_USER.id, chat_id=TEST_USER_CHAT.id)
    )

    await process_decline(message, state=state)

    message.answer.assert_called_with('Ок, отменяем. Представьтесь', reply_markup=ReplyKeyboardRemove())

    assert await state.get_data() == {}
    assert await state.get_state() == help_state.HelpState.name.state

# after submit is integration test!



