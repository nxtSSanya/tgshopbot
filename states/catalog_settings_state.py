from aiogram.fsm.state import StatesGroup, State


class CatalogSettingsState(StatesGroup):
    """
    Класс для хранения состояний при настройке каталога

    Атрибуты:
    ----------

    choice: начальное состояние, выбор: что делать с каталогом

    name: состояние для названия товара

    category: состояние для ввода категории товара

    body: состояние для ввода описания товара

    quantity: состояние для ввода количества товара

    photo: состояние для загрузки фотографии товара

    price: состояние для ввода цены товара

    is_confirm: состояние для подтверждения информации
    """

    choice = State()
    name = State()
    category = State()
    body = State()
    quantity = State()
    photo = State()
    price = State()
    is_confirm = State()
