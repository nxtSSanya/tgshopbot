from aiogram.filters.state import State, StatesGroup


class OrderInfoState(StatesGroup):
    """
    Класс для хранения состояний при получении информации о заказе

    Атрибуты:
    ---------

    order_id: начальное состояние для ввода ID заказа
    """

    order_id = State()
    confirm = State()
    confirmed = State()
