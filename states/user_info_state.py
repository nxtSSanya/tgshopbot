from aiogram.filters.state import State, StatesGroup


class UserInfoState(StatesGroup):
    """
    Класс для хранения состояний при оформлении заказа

    Атрибуты:
    ----------

    name: начальное состояние, пользователь вводит своё имя

    phone: состояние для ввода номера телефона

    address: состояние для ввода адреса

    confirm: состояние для подтверждения информации
    """

    name = State()
    phone = State()
    address = State()
    confirm = State()
    confirmed = State()
