from aiogram.filters.state import State, StatesGroup


class HelpState(StatesGroup):
    """
    Класс для хранения состояний при задании вопросов от пользователя

    Атрибуты:
    ----------

    name: начальное состояние, пользователь вводит своё имя

    phone: состояние для ввода номера телефона

    question: состояние для ввода вопроса

    submit: состояние для подтверждения информации
    """

    question = State()
    name = State()
    phone = State()
    submit = State()
