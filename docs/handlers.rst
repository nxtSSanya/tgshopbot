handlers
============================

.. py:module:: handlers

.. toctree::
   :maxdepth: 4

   handlers.admin_mode
   handlers.user_mode

order_info
----------------------------------------------

.. automodule:: handlers.order_info
   :members:
   :undoc-members:
   :show-inheritance:
