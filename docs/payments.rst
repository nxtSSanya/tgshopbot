payments
============================

.. py:module:: payments

.. automodule:: payments.yokassa_payment
   :members:
   :undoc-members:
   :show-inheritance:
