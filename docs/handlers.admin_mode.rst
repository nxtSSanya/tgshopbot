admin\_mode
========================================

.. py:module:: handlers.admin_mode

catalog_settings
___________________________________________________________

.. automodule:: handlers.admin_mode.catalog_settings
   :members:
   :undoc-members:
   :show-inheritance:

orders
___________________________________________________________

.. automodule:: handlers.admin_mode.orders
   :members:
   :undoc-members:
   :show-inheritance:

questions
___________________________________________________________

.. automodule:: handlers.admin_mode.questions
   :members:
   :undoc-members:
   :show-inheritance:
