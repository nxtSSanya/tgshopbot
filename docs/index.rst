.. tgshopbot documentation master file, created by
   sphinx-quickstart on Mon Nov 20 17:07:02 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to TelegramShopBot's documentation!
============================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   tgshopbot


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
