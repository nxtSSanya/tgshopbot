user\_mode
=======================================

.. py:module:: handlers.user_mode

.. toctree::
   :maxdepth: 4

   handlers.user_mode.offer

cart
__________________________________________________

.. automodule:: handlers.user_mode.cart
   :members:
   :undoc-members:
   :show-inheritance:

catalog
__________________________________________________

.. automodule:: handlers.user_mode.catalog
   :members:
   :undoc-members:
   :show-inheritance:

help
__________________________________________________

.. automodule:: handlers.user_mode.help
   :members:
   :undoc-members:
   :show-inheritance:

mode_menu
__________________________________________________

.. automodule:: handlers.user_mode.mode_menu
   :members:
   :undoc-members:
   :show-inheritance:

users_orders
__________________________________________________

.. automodule:: handlers.user_mode.users_orders
   :members:
   :undoc-members:
   :show-inheritance:
