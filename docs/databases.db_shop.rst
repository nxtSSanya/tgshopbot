tables
======================================

.. py:module:: databases.db_shop

.. toctree::
   :maxdepth: 4

.. automodule:: databases.db_shop.tables.base_table
   :members:
   :undoc-members:

.. automodule:: databases.db_shop.tables.cart
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: databases.db_shop.tables.categories
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: databases.db_shop.tables.help_requests
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: databases.db_shop.tables.orders
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: databases.db_shop.tables.products
   :members:
   :undoc-members:
   :show-inheritance:
