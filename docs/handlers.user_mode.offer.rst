offer
=============================================

.. py:module:: handlers.user_mode.offer

offer
------------------------------------------------

.. automodule:: handlers.user_mode.offer.offer
   :members:
   :undoc-members:
   :show-inheritance:

offer\_bl
----------------------------------------------------

.. automodule:: handlers.user_mode.offer.offer_bl
   :members:
   :undoc-members:
   :show-inheritance:
