enums
=========================

.. py:module:: enums

.. automodule:: enums.inform_type
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: enums.payment_statuses
   :members:
   :undoc-members:
   :show-inheritance:
