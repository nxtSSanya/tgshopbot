all_states = ["Все верно", "Отменяем"]
available_confirm_states = ["Все верно"]
available_decline_states = ["Отменяем"]
pay_all_state = ["Оплатить онлайн", "Оплатить наличными", "Я передумал"]
pay_accept_state = ["Оплатить онлайн", "Оплатить наличными"]
pay_decline_state = ["Я передумал"]

add_product_state = "Добавить"
remove_product_state = "Удалить"
select_type_setting = [add_product_state, remove_product_state] + available_decline_states
