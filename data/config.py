class Config:
    """
    Класс содержит конфигурационные данные

    Атрибуты
    --------
    BOT_TOKEN: токен телеграм-бота для взаимодействия с ним

    ADMINS: список ID пользователей, которые являются админами/менеджерами

    SECRET_KEY: токен магазина ЮKassa

    ACCOUNT_ID: ID магазина ЮKassa

    BOT_URL: URL телеграм бота для редиректа после оплаты в ЮKassa

    PAYMENT_TIMEOUT_MINUTE: время на онлайн-оплату

    CHECK_PAYMENT_INTERVAL_SECONDS: интервал, через который проверяется онлайн-оплата

    CURRENCY: валюта оплаты

    PAYMENT_TYPE: способ оплаты

    CONFIRMATION_TYPE: способ подтверждения платежа

    IS_CAPTURE: принимать ли платёж автоматически

    DB_PATH: абсолютный путь к базе данных магазина
    """
    BOT_TOKEN = '6488387601:AAGY3JstroCmCkP-KRMGWuJ1Qo-OP-9-IqY'
    ADMINS = [984072088, 854673388]

    SECRET_KEY = "test_2Orlz2mv5zcR6mvKiUsPodhBcn6b5RTDFTj_7WWdqc8"
    ACCOUNT_ID = "272411"
    BOT_URL = "https://t.me/sound_panels_tg_shop_bot"
    PAYMENT_TIMEOUT_MINUTE = 15
    CHECK_PAYMENT_INTERVAL_SECONDS = 5

    CURRENCY = "RUB"
    PAYMENT_TYPE = "bank_card"
    CONFIRMATION_TYPE = "redirect"
    IS_CAPTURE = True

    DB_PATH = r""  # absolute path to db
