from aiogram.filters import BaseFilter
from aiogram.types import Message

from data.config import Config


class IsUser(BaseFilter):
    """Класс, наследующийся от BaseFilter. Нужен для фильтрации пользователей"""

    async def __call__(self, message: Message) -> bool:
        return message.from_user.id not in Config.ADMINS
