from aiogram.filters import BaseFilter
from aiogram.types import Message

from data.config import Config


class IsAdmin(BaseFilter):
    """Класс, наследующийся от BaseFilter. Нужен для фильтрации администраторов"""

    async def __call__(self, message: Message) -> bool:
        return message.from_user.id in Config.ADMINS
