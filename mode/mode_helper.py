from data.config import Config


async def is_admin(uid: int) -> bool:
    """
    Функция для проверки пользователя: является ли он администратором

    :param uid: ID пользователя
    :return: True - пользователь является администратром
    """
    if uid in Config.ADMINS:
        return True
    else:
        return False


async def is_user(uid: int) -> bool:
    """
    Функция для проверки пользователя: является ли он обычным пользователем

    :param uid: ID пользователя
    :return: True - пользователь является обычным пользователем
    """
    if uid not in Config.ADMINS:
        return True
    else:
        return False
