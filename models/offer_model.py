class OfferModel:
    """
    Класс для хранения информации о заказе

    Атрибуты:
    ---------

    name (str): имя пользователя

    address (str): адрес пользователя

    phone (str): номер телефона пользователя

    payment_id (str): ID платежа (если оплата наличными, то равно '')

    is_delivery (bool): заказ с доставкой?

    order_id (str): ID заказа
    """

    name: str
    address: str
    phone: str
    payment_id: str
    is_delivery: bool
    order_id: str
