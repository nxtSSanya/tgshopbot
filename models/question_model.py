from utilities.string_utility import StringUtility


class QuestionModel:
    """
    Класс для хранения информации о вопросе пользователя

    Атрибуты:
    ----------

    name (str): имя пользователя

    phone (str): телефон пользователя

    question (str): вопрос пользователя
    """

    name: str
    phone: str
    question: str

    def __str__(self):
        return f'{StringUtility.bold_text("Имя:")} {self.name}\n\n' \
               f'{StringUtility.bold_text("Номер телефона:")} {self.phone}\n\n' \
               f'{StringUtility.bold_text("Вопрос:")} {self.question}'
