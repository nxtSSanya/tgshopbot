from sqlalchemy.orm import Mapped
from sqlalchemy.orm import mapped_column
from sqlalchemy.types import BLOB

from databases.db_shop.tables.base_table import Base
from utilities.string_utility import StringUtility


class Products(Base):
    """
    Класс для ORM представления таблицы "Products"

    Атрибуты
    --------

    product_id: id строки, первичный ключ

    type: категория продукта

    name: название продукта

    body: описание продукта

    quantity: количество продукта на складе

    photo: фото продукта

    price: цена за единицу продукта
    """
    __tablename__ = "Products"

    product_id: Mapped[int] = mapped_column(primary_key=True, nullable=False, unique=True, autoincrement=True)
    type: Mapped[str] = mapped_column(nullable=False)
    name: Mapped[str] = mapped_column(nullable=False, unique=True)
    body: Mapped[str] = mapped_column(nullable=False)
    quantity: Mapped[int] = mapped_column(nullable=False)
    photo: Mapped[bytes] = mapped_column(BLOB, nullable=False)
    price: Mapped[int] = mapped_column(nullable=False)

    def __str__(self):
        return f'{StringUtility.bold_text("Название:")} {self.name} \n\n' \
               f'{StringUtility.bold_text("Категория:")} {self.type}\n\n' \
               f'{StringUtility.bold_text("Описание:")} {self.body}\n\n' \
               f'{StringUtility.bold_text("Количество:")} {self.quantity}\n\n' \
               f'{StringUtility.bold_text("Цена:")} {self.price}\n\n'
