from datetime import datetime
from typing import Optional

from sqlalchemy.orm import Mapped, mapped_column

from databases.db_shop.tables.base_table import Base


class Orders(Base):
    """
    Класс для ORM представления таблицы "Orders"

    Атрибуты
    --------

    order_id: id строки, первичный ключ

    user_id: id пользователя, который сделал заказ

    payment_id: ключ платежа при онлайн оплате, иначе равен ''

    name: имя, которое ввёл пользователь

    address: адрес, который вёл пользователь, если выбрал доставку (иначе '')

    payment_amount: стоимость заказа

    date: дата и время оформления заказа
    """
    __tablename__ = "Orders"

    order_id: Mapped[int] = mapped_column(primary_key=True, nullable=False, unique=True, autoincrement=True)
    user_id: Mapped[int] = mapped_column(nullable=False)
    payment_id: Mapped[Optional[int]]
    name: Mapped[str] = mapped_column(nullable=False)
    phone: Mapped[str] = mapped_column(nullable=False)
    address: Mapped[Optional[int]]
    payment_amount: Mapped[int] = mapped_column(nullable=False)
    date: Mapped[datetime] = mapped_column(nullable=False)
