from sqlalchemy.orm import Mapped, mapped_column

from databases.db_shop.tables.base_table import Base


class Cart(Base):
    """
    Класс для ORM представления таблицы "Cart"

    Атрибуты
    --------

    cart_id: id строки, первичный ключ

    user_id: id пользователя, кому принадлежит товар в корзине

    product_id: id продукта в корзине

    quantity: количество данного продукта в корзине

    price: цена за единицу товара

    order_id: номер заказа, если сделан заказ, иначе равен нулю (пользователь ещё выбирает, что купить)
    """
    __tablename__ = "Cart"

    cart_id: Mapped[int] = mapped_column(primary_key=True, nullable=False, unique=True, autoincrement=True)
    user_id: Mapped[int] = mapped_column(nullable=False)
    product_id: Mapped[int] = mapped_column(nullable=False)
    quantity: Mapped[int] = mapped_column(nullable=False)
    price: Mapped[int] = mapped_column(nullable=False)
    order_id: Mapped[int] = mapped_column(nullable=False)
