from datetime import datetime

from sqlalchemy.orm import Mapped, mapped_column

from databases.db_shop.tables.base_table import Base
from utilities.string_utility import StringUtility


class HelpRequests(Base):
    """
    Класс для ORM представления таблицы "HelpRequests"

    Атрибуты
    --------

    message_id: id строки, первичный ключ

    user_id: id пользователя, который задал вопрос

    message: вопрос, который задал пользователь

    name: имя, которым представился пользователь

    phone: номер телефона, который предоставил пользователь

    date: дата и время, когда задали вопрос
    """
    __tablename__ = "HelpRequests"

    message_id: Mapped[int] = mapped_column(primary_key=True, nullable=False, unique=True, autoincrement=True)
    user_id: Mapped[int] = mapped_column(nullable=False)
    message: Mapped[str] = mapped_column(nullable=False)
    name: Mapped[str] = mapped_column(nullable=False)
    phone: Mapped[str] = mapped_column(nullable=False)
    date: Mapped[datetime] = mapped_column(nullable=False)

    def __str__(self):
        return f"{StringUtility.bold_text('Пользователь')}: {self.user_id}\n" \
               f"{StringUtility.bold_text('Имя')}: {self.name}\n" \
               f"{StringUtility.bold_text('Телефон')}: {self.phone}\n" \
               f"{StringUtility.bold_text('Дата и время отправки')}: {self.date}\n" \
               f"{StringUtility.bold_text('Сообщение')}: '{self.message}'"
