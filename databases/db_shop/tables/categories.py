from sqlalchemy.orm import Mapped, mapped_column

from databases.db_shop.tables.base_table import Base


class Categories(Base):
    """
    Класс для ORM представления таблицы "Categories"

    Атрибуты
    --------

    category_id: id строки, первичный ключ

    title: имя категории
    """
    __tablename__ = "Categories"

    category_id: Mapped[int] = mapped_column(primary_key=True, nullable=False, unique=True, autoincrement=True)
    title: Mapped[str] = mapped_column(nullable=False)
