from sqlalchemy.orm import DeclarativeBase


class Base(DeclarativeBase):
    """Базовый класс-заглушка для создания ORM представлений"""
    pass
