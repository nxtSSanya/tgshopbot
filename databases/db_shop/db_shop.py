from datetime import datetime
from typing import Sequence

import sqlalchemy as db
from sqlalchemy import select
from sqlalchemy.orm import Session
from sqlalchemy.sql import func

from data.config import Config
from databases.db_shop.tables.cart import Cart
from databases.db_shop.tables.categories import Categories
from databases.db_shop.tables.help_requests import HelpRequests
from databases.db_shop.tables.orders import Orders
from databases.db_shop.tables.products import Products
from utilities.random_utility import RandomUtility


class DbShop:
    """
    Класс для работы с базой данных магазина
    """

    def __init__(self):
        self.__engine = db.create_engine(f"sqlite:///{Config.DB_PATH}")
        self.__session = Session(self.__engine)

    # region Products

    def get_all_products(self) -> Sequence[Products]:
        """
        Возвращает список всех продуктов

        :return: список продуктов
        """
        return self.__session.scalars(select(Products)).all()

    def get_product_by_id(self, product_id: int) -> Products:
        """
        Возвращает продукт по его ID

        :param product_id: ID продукта
        :return: продукт
        """
        return self.__session.scalars(select(Products).where(Products.product_id == product_id)).one()

    def add_new_product(self, product: Products):
        """
        Добавляет в базу данных новый продукт

        :param product: новый продукт
        """
        self.__session.add(product)
        self.__session.flush()
        self.__session.commit()

    def update_count_by_name(self, name: str, new_count: int):
        """
        Обновляет количество продукта на складе

        :param name: имя продукта
        :param new_count: новое количество товара
        """
        self.__session.scalars(select(Products).where(Products.name == name)).one().count = new_count
        self.__session.flush()
        self.__session.commit()

    def delete_product_by_id(self, product_id: int):
        """
        Удаляет продукт по ID

        :param product_id: ID удаляемого продукта
        """
        product = self.__session.scalars(select(Products).where(Products.product_id == product_id)).one()
        self.__session.delete(product)
        self.__session.flush()
        self.__session.commit()

    # endregion

    # region Categories

    def get_all_categories(self) -> Sequence[Categories]:
        """
        Возвращает список всех категорий

        :return: список категорий
        """
        return self.__session.scalars(select(Categories)).all()

    # endregion

    # region Cart

    def get_all_cart_items(self, user_id: int) -> Sequence[Cart]:
        """
        Возвращает корзину определённого пользователя

        :param user_id: ID пользователя
        :return: список продуктов в корзине
        """
        return self.__session.scalars(select(Cart).where(Cart.user_id == user_id)).all()

    def get_not_ordered_items(self, user_id: int) -> Sequence[Cart]:
        """
        Возвращает корзину незаказанных товаров пользователя

        :param user_id: ID пользователя
        :return: список незаказанных продуктов в корзине
        """
        return self.__session.scalars(select(Cart).where(Cart.user_id == user_id).where(Cart.order_id == 0)).all()

    def get_products_from_not_cart(self, cat_id: int, user_id: int) -> Sequence[Products]:
        """
        Возвращает список продуктов, которых нет в корзине пользователя

        :param cat_id: ID категории продукта
        :param user_id: ID пользователя
        :return: список продуктов
        """
        return self.__session.scalars(
            select(Products).
            join(Categories, Products.type == Categories.title)
            .where(Categories.category_id == cat_id)
            .where(Products.quantity > 0)
            .where(Products.product_id.notin_(
                select(Cart.product_id).where(Cart.user_id == user_id).where(Cart.order_id == 0)))).all()

    def add_cart_item(self, user_id: int, product_id: int, price: int):
        """
        Добавляет новый продукт в корзину

        :param user_id: ID пользователя
        :param product_id: ID продукта
        :param price: цена продукта
        """
        self.__session.add(Cart(user_id=user_id, product_id=product_id, quantity=1, price=price, order_id=0))
        self.__session.flush()
        self.__session.commit()

    def clear_cart(self, user_id: int):
        """
        Очищает корзину выбранного пользователя

        :param user_id: ID пользователя
        """
        for item in self.get_all_cart_items(user_id):
            self.__session.delete(item)
        self.__session.flush()
        self.__session.commit()

    def update_cart(self, product_id: int, user_id: int, quantity: int):
        """
        Обновляет количество товара в корзине

        :param product_id: ID продукта
        :param user_id: ID пользователя
        :param quantity: новое количество
        """
        for item in self.get_all_cart_items(user_id):
            if item.order_id == 0 and item.product_id == product_id:
                item.quantity = quantity
                break
        self.__session.flush()
        self.__session.commit()

    def delete_from_cart_by_user_id(self, product_id: int, user_id: int):
        """
        Удаляет продукт из корзины пользователя

        :param product_id: ID продукта
        :param user_id: ID пользователя
        """
        for item in self.get_all_cart_items(user_id):
            if item.order_id == 0 and item.product_id == product_id:
                self.__session.delete(item)
                break
        self.__session.flush()
        self.__session.commit()

    def get_cart_cost(self, user_id: int, order_id=0) -> int:
        """
        Возвращает стоимость корзины пользователя

        :param user_id: ID пользователя
        :param order_id: ID заказа (по умолчанию = 0, т.е. ДО заказа)
        :return: стоимость товаров в корзине
        """
        return self.__session.scalars(select(func.sum(Cart.quantity * Cart.price)).where(Cart.user_id == user_id)
                                      .where(Cart.order_id == order_id)).one()

    def make_cart_ordered(self, user_id: int, order_id: str):
        """
        Выполняется после заказа. Помечает все товары в корзине, как заказанные (изменяет order_id на не равное 0)

        :param user_id: ID пользователя
        :param order_id: ID заказа
        """
        for item in self.get_all_cart_items(user_id):
            if item.user_id == user_id and item.order_id == 0:
                item.order_id = order_id
        self.__session.flush()
        self.__session.commit()

    def update_quantity_after_offer(self, user_id: int):
        """
        Обновляет количество товара на складе после заказа

        :param user_id: ID пользователя
        """
        products_quantity = dict()
        for item in self.get_all_cart_items(user_id):
            if item.user_id == user_id and item.order_id == 0:
                products_quantity[item.product_id] = item.quantity

        for product in self.get_all_products():
            if product.product_id in products_quantity:
                product.quantity -= products_quantity[product.product_id]

        self.__session.flush()
        self.__session.commit()

    def get_cart_items_by_order_id(self, order_id: str) -> Sequence[Cart]:
        """
        Возвращает список продуктов из корзины по order_id

        :param order_id: ID заказа
        :return: список продуктов из корзины
        """
        return self.__session.scalars(select(Cart).where(Cart.order_id == order_id)).all()

    # endregion

    # region Orders

    def get_all_orders(self) -> Sequence[Orders]:
        """
        Возвращает список всех заказов

        :return: список заказов
        """
        return self.__session.scalars(select(Orders)).all()

    def add_new_order(self, name: str, phone: str, address: str, user_id: int, payment_id: str,
                      payment_amount: int, date: datetime) -> str:
        """
        Добавляет новый заказ

        :param name: имя пользователя
        :param phone: номер телефона пользователя
        :param address: адрес пользователя (если НЕ доставка, то '')
        :param user_id: ID пользователя
        :param payment_id: ID платежа, если онлайн оплата, иначе ''
        :param payment_amount: стоимость заказа
        :param date: дата и время заказа
        :return: уникальный ID заказа
        """
        order = Orders(order_id=RandomUtility.get_order_id(),
                       user_id=user_id,
                       payment_id=payment_id,
                       name=name,
                       phone=phone,
                       address=address,
                       payment_amount=payment_amount,
                       date=date)
        self.__session.add(order)
        self.__session.flush()
        self.__session.commit()
        return order.order_id

    # endregion

    # region HelpRequests

    def add_new_message(self, user_id: int, name: str, phone: str, message: str, date: datetime):
        """
        Добавляет новый вопрос от пользователя

        :param user_id: ID пользователя
        :param name: имя пользователя
        :param phone: номер телефона пользователя
        :param message: вопрос пользователя
        :param date: дата и время вопроса
        """
        message = HelpRequests(user_id=user_id, message=message, name=name, phone=phone, date=date)
        self.__session.add(message)
        self.__session.flush()
        self.__session.commit()

    def get_all_questions(self) -> Sequence[HelpRequests]:
        """
        Возвращает список всех вопросов

        :return: список вопросов
        """
        return self.__session.scalars(select(HelpRequests)).all()

    # endregion
